import React from 'react';
import styled from "styled-components";
import {PageErrorLayout} from "../components/Layouts/PageErrorLayout";
import {withRedux} from "../redux/withRedux";

const Error = props => {
    const {statusCode} = props;

    return (
        <FirstSection data-id="main">
            <div className="wrap">
                <div className="company-name">
                    <div className="company-name__item">
                        <div>4</div>
                    </div>
                    <div className="company-name__item">
                        <div>0</div>
                    </div>
                    <div className="company-name__item">
                        <div>4</div>
                    </div>
                </div>

                <div className="content-bottom">
                    Страница не найдена
                </div>
            </div>
        </FirstSection>
    );
};

Error.getInitialProps = ({res, err}) => {
    const statusCode = res ? res.statusCode : err ? err.statusCode : null;
    return {statusCode};
};

const FirstSection = styled('div')`
    min-height: calc(var(--vh, 1vh) * 100);
    position: relative;
    background: url("/assets/images/first-section-bg.jpg") no-repeat center -262px;
    background-size: auto;
    color: #fff;
    overflow-x: hidden;
    box-sizing: border-box;
    .company-name {
        display: flex;
        padding: 0 0 0 26px;
        margin-bottom: 60px;
        margin-right: -44px;
        box-sizing: border-box;
        &__item {
            display: flex;
            width: 100%;
            padding-top: 146px;
            border-left: 1px solid #96C2EB;
            font-size: 195px;
            font-family: 'Circe-800';
            line-height: 103px;
            overflow: hidden;
            box-sizing: border-box;
            &:nth-child(1) {
                div {
                    transform: translate(-27px, 0);
                }
            }
            &:nth-child(2) {
                div {
                    transform: translate(-24px, 0);
                }
            }
            &:nth-child(3) {
                div {
                    transform: translate(-22px, 0);
                } 
            }
        }
    }
    .content-bottom {
        text-align: center;
        font-family: 'Circe-300';
        font-size: 28px;
        line-height: 120%;
    }
    @media (min-width: 1024px) {
        .company-name {
            width: initial;
            padding: 0 0 0 38px;
            margin-right: -15px;
            &__item {
                margin-right: 10%;
                padding-right: 0;
                padding-top: 193px;
            }
        }
        .content-bottom {
            font-size: 50px;
        }
    }
    @media (min-width: 1280px) {
        padding-bottom: 133px;
        .company-name {
            padding: 0 0 0 119px;
            margin-right: 58px;
        }
        .content-bottom {
            padding: 65px 22px 0 119px;
        }
        .col-item__title {
            margin-bottom: 5px;
        }
        .col-item {
            margin-bottom: 45px;
        }
        .col:nth-child(1) {
            width: 31%;
        }
        .col:nth-child(2) {
            width: 81%;
        }
        .col__title {
            font-size: 34px;
            line-height: 120%;
        }
        .col__subtitle {
            width: 73%;
        }
    }
    @media (min-width: 1600px) {
        padding-bottom: 193px;
        background-position: center -166px;
        .company-name {
            padding: 0 0 0 158px;
            margin-right: 117px;
            &__item {
                font-size: 300px;
                padding-top: 289px;
                line-height: 148px;
                &:nth-child(1) {
                    div {
                        transform: translate(-45px, 0);
                    }
                }
                &:nth-child(3) {
                    div {
                        transform: translate(-39px, 0);
                    }
                }
            }
        }
    }
    @media (min-width: 1920px) {
        background-size: 102% auto;
        background-position: -1px -188px;
    }
`;

export default withRedux(Error, {classes: ['page-error'], Layout: PageErrorLayout, fetchURLLayout: null});