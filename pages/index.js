import React from 'react';
import {withRedux} from '../redux/withRedux';
import Head from "next/head";
import {FeedbackForm} from "../components/FeedbackForm";
import {FirstSection} from "../components/styled-page/IndexPage/FirstSection/FirstSection";
import {ConsumerSection} from "../components/styled-page/IndexPage/ConsumerSection/ConsumerSection";
import {AboutProductSection} from "../components/styled-page/IndexPage/AboutProductSection/AboutProductSection";
import {ForWhomSection} from "../components/styled-page/IndexPage/ForWhomSection/ForWhomSection";
import {ProjectSpecificationSection} from "../components/styled-page/IndexPage/ProjectSpecificationSection/ProjectSpecificationSection";
import {ContactsSection} from "../components/styled-page/IndexPage/ContactsSection/ContactsSection";
import {SanctionSection} from "../components/styled-page/IndexPage/SanctionSection/SanctionSection";

const Index = () => {
  return (
    <>
      <Head>
        <meta name="description" content="Снижение платы за электроэнергию и мощность для промышленных потребителей"/>
      </Head>
      <FirstSection/>
      <ConsumerSection/>
      <SanctionSection/>
      <AboutProductSection/>
      <ForWhomSection/>
      <ProjectSpecificationSection/>
      <FeedbackForm/>
      <ContactsSection/>
    </>
  )
};

export default withRedux(Index, {classes: ['page-index'], fetchURLLayout: null});