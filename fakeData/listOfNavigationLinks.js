export const listOfNavigationLinksHeader = [
    {
        label: 'Главная',
        anchorName: 'main',
        hashName: '/'
    },
    {
        label: 'Постановление',
        anchorName: 'act',
        hashName: '/#постановление'
    },
    {
        label: 'О продукте',
        anchorName: 'about',
        hashName: '/#о-продукте'
    },
    {
        label: 'Для кого',
        anchorName: 'for-whom',
        hashName: '/#для-кого'
    },
    {
        label: 'Технические требования',
        anchorName: 'project-specification',
        hashName: '/#технические-требования'
    }
];

export const listOfNavigationLinksFooter = [
    {
        label: 'Главная',
        anchorName: 'main',
        hashName: '/'
    },
    {
        label: 'О продукте',
        anchorName: 'about',
        hashName: '/#о-продукте'
    },
    {
        label: 'Для кого',
        anchorName: 'for-whom',
        hashName: '/#для-кого'
    },
    {
        label: 'Технические требования',
        anchorName: 'project-specification',
        hashName: '/#технические-требования'
    },
    {
        label: 'Контакты',
        anchorName: 'contacts',
        hashName: '/#контакты'
    }
];