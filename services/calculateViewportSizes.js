import React, {useEffect} from "react";

export const CalculateViewportSizes = props => {
    const {children} = props;

    const setVh = () => {
        let vh = window.innerHeight * 0.01;
        let vw = document.documentElement.clientWidth || document.body.clientWidth;
        document.documentElement.style.setProperty("--vh", `${vh}px`);
        document.documentElement.style.setProperty("--vw", `${vw}px`);
    };

    useEffect(() => {
        setVh();
        window.addEventListener("resize", setVh);

        return () => {
            window.removeEventListener("resize", setVh);
        }
    });

    return (
        <>
            {children}
        </>
    )
};

/* Use simple */
// top: calc(var(--vh, 1vh) * 90);