import {actionTypes} from "../actions/actionTypes";

const initialState = {
    isOpenMenu: false
};

export const mainApp = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.TOGGLE_PAGE_MENU:
            return {...state, isOpenMenu: action.isOpen};
        default:
            return state
    }
};