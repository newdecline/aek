import { combineReducers } from "redux";
import { mainApp } from './mainApp';

export default combineReducers({
    app: mainApp
});