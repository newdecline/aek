import {actionTypes} from "../actions/actionTypes";

export const togglePageMenu = (isOpen) => {
    return { type: actionTypes.TOGGLE_PAGE_MENU, isOpen }
};