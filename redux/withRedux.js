import React from 'react';
import App from 'next/app';
import {Provider} from 'react-redux';
import {initializeStore} from './store';
import { createGlobalStyle } from "styled-components";
import { reset } from 'styled-reset';
import {MainLayout} from "../components/Layouts/MainLayout";
import {fetchApi} from "../services/api/fetchApi";
import {CalculateViewportSizes} from "../services/calculateViewportSizes";
import {pick} from 'lodash';

export const withRedux = (
    PageComponent, {ssr = true, Layout = MainLayout, fetchURLLayout = '/layout', classes = []} = {}) => {

    const WithRedux = ({initialReduxState, ...props}) => {
        if (PageComponent.name !== 'Error' && props.error && props.error.statusCode === 404) {
            const err = new Error();
            err.statusCode = 404;
            err.code = 'ENOENT';
            throw err;
        }

        const store = getOrInitializeStore(initialReduxState);

        if (!Layout) {
            return (
                <Provider store={store}>
                    <CalculateViewportSizes>
                        <div className={classes.join(' ')}>
                            <GlobalStyles />
                            <PageComponent {...props} />
                        </div>
                    </CalculateViewportSizes>
                </Provider>
            )
        }

        return (
            <Provider store={store}>
                <CalculateViewportSizes>
                    <div className={classes.join(' ')}>
                        <Layout {...pick(props, 'layoutData')}>
                            <GlobalStyles />
                            <PageComponent {...props} />
                        </Layout>
                    </div>
                </CalculateViewportSizes>
            </Provider>
        )
    };

    if (process.env.NODE_ENV !== 'production') {
        const isAppHoc =
            PageComponent === App || PageComponent.prototype instanceof App;
        if (isAppHoc) {
            throw new Error('The withRedux HOC only works with PageComponents')
        }
    }

    if (process.env.NODE_ENV !== 'production') {
        const displayName =
            PageComponent.displayName || PageComponent.name || 'Component';

        WithRedux.displayName = `withRedux(${displayName})`
    }

    if (ssr || PageComponent.getInitialProps) {
        WithRedux.getInitialProps = async context => {
            const reduxStore = getOrInitializeStore();

            context.reduxStore = reduxStore;

            let pageProps =
                typeof PageComponent.getInitialProps === 'function'
                    ? await PageComponent.getInitialProps(context)
                    : {};

            if (fetchURLLayout) {
                return fetchApi(`${fetchURLLayout}`, {
                    query: {
                        fields: `*, homePage.show_about_section, homePage.show_events_section`,
                        expand: 'customCode, homePage, homePage.areas'
                    }
                }).then(res => {
                        return {
                            ...pageProps,
                            layoutData: res.data,
                            initialReduxState: reduxStore.getState()
                        }
                    }
                );
            } else {
                return {
                    ...pageProps,
                    initialReduxState: reduxStore.getState()
                }
            }
        }
    }

    return WithRedux;
};

let reduxStore;

const getOrInitializeStore = initialState => {
    if (typeof window === 'undefined') {
        return initializeStore(initialState)
    }

    if (!reduxStore) {
        reduxStore = initializeStore(initialState)
    }

    return reduxStore
};

const GlobalStyles = createGlobalStyle`
    ${reset}
    
    @font-face {
        font-family: 'Circe-300';
        src: url('/assets/fonts/Circe-Light.woff') format("woff");
        font-weight: 300;
        font-style: normal;
    }
    
    @font-face {
        font-family: 'Circe-400';
        src: url('/assets/fonts/Circe-Regular.woff') format("woff");
        font-weight: 400;
        font-style: normal;
    }
    
    @font-face {
        font-family: 'Circe-700';
        src: url('/assets/fonts/Circe-Bold.woff') format("woff");
        font-weight: 700;
        font-style: normal;
    }
    
    @font-face {
        font-family: 'Circe-800';
        src: url('/assets/fonts/Circe-ExtraBold.woff') format("woff");
        font-weight: 800;
        font-style: normal;
    }
    
    body {
        font-family: 'Circe-400';
    }
    
    img {
        max-width: 100%;
        width: 100%;
        display: block;
    }
    
    [class*="ymaps-2"][class*="-ground-pane"] {
        filter: grayscale(100%);
    }
    
    .map {
        height: 337px;
    }
    
    @media (min-width: 1024px) {
        .map {
            height: 385px;
        }
    }
    
    @media (min-width: 1280px) {
        .map {
            height: 355px;
        }
    }
    
    @media (min-width: 1600px) {
        .map {
            height: 637px;
        }
    }
`;