import React, {useState, useEffect, useRef} from 'react';
import Link from 'next/link';
import styled, {keyframes} from 'styled-components';
import * as Yup from 'yup';
import {Formik} from "formik";
import {PortalWithState} from 'react-portal';
import ReCAPTCHA from "react-google-recaptcha";
import {enablePageScroll, disablePageScroll} from 'scroll-lock';
import {fetchApi} from "../services/api/fetchApi";
import {config} from "../config";
import CrossIcon from "../svg/cross.svg";

const FeedbackSchema = Yup.object().shape({
    name: Yup.string().required(),
    organisation: Yup.string(),
    post: Yup.string(),
    phone: Yup.string().required(),
    email: Yup.string().required(),
    captcha: Yup.string().required(),
    agreement: Yup.bool().oneOf([true])
});

export const FeedbackForm = () => {
    const gReCaptchaRef = useRef(null);

    const [isSendForm, setSendForm] = useState(false);
    const [isSubmitting, setSubmitting] = useState(false);

    const onClosePopup = (closePortal) => {
        closePortal();
        setSendForm(false);
    };

    useEffect(() => {
        if (isSendForm) {
            disablePageScroll();

            if (gReCaptchaRef.current) {
                gReCaptchaRef.current.reset();
            }
        } else {
            enablePageScroll();
        }
    }, [isSendForm]);

    return (
        <PortalWithState closeOnOutsideClick closeOnEsc>
            {({openPortal, closePortal, isOpen, portal}) => (
                <>
                    <Formik
                        initialValues={{
                            name: '',
                            organisation: '',
                            post: '',
                            phone: '',
                            email: '',
                            captcha: '',
                            agreement: false
                        }}

                        validationSchema={FeedbackSchema}

                        onSubmit={(values, {resetForm}) => {
                            const data = {...values};
                            delete data.agreement;

                            setSubmitting(true);

                            fetchApi('/public/feedbacks', {
                                method: 'post',
                                body: JSON.stringify(data),
                            })
                                .then(() => {
                                    setSendForm(true);
                                    openPortal();
                                    resetForm();
                                    setSubmitting(false);
                                })
                                .catch(err => {
                                    resetForm();
                                    setSubmitting(false);
                                    console.log(err);
                                    alert('Форма не отправлена, попробуйте позже');
                                });
                        }}
                    >
                        {props => {
                            const {
                                values,
                                errors,
                                touched,
                                handleBlur,
                                handleChange,
                                handleSubmit,
                            } = props;

                            return (
                                <FeedbackFormStyled id='получить-консультацию' data-id={'form'}>
                                    <div className="wrap">
                                        <div className="section-title">Получить <br/> консультацию</div>
                                        <div className="section-subtitle">АЭК <br/> ПАК УИС</div>
                                    </div>

                                    <StyledForm onSubmit={handleSubmit}>
                                        <input
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.name}
                                            name="name"
                                            placeholder="Фамилия Имя Отчество"
                                            className={errors.name && touched.name ? 'name input input_err' : 'name input'}/>
                                        <input
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.organisation}
                                            name="organisation"
                                            placeholder="Название организации"
                                            className={errors.organisation && touched.organisation ? 'organisation input input_err' : 'organisation input'}/>
                                        <input
                                            type="phone"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.phone}
                                            name="phone"
                                            placeholder="Телефон"
                                            className={errors.phone && touched.phone ? 'phone input input_err' : 'phone input'}/>
                                        <input
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.post}
                                            name="post"
                                            placeholder="Должность"
                                            className={errors.post && touched.post ? 'post input input_err' : 'post input'}/>
                                        <input
                                            type="text"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.email}
                                            name="email"
                                            placeholder="E-mail"
                                            className={errors.email && touched.email ? 'email input input_err' : 'email input'}/>
                                        <label
                                            className={errors.agreement && touched.agreement ? 'agreement label label_err' : 'agreement label'}>
                                            <input
                                                name="agreement"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.agreement}
                                                checked={values.agreement}
                                                type="checkbox"
                                                className="label__checkbox"/>
                                            <div className='label__icon'></div>
                                            <span className="label__text">С политикой <Link href="/agreement"><a target="_blank">обработки персональных данных</a></Link> согласен&#8260;согласна</span>
                                        </label>

                                        <div className="captcha-wrap">
                                            <ReCAPTCHA
                                                sitekey={config.siteKeyCaptcha}
                                                theme="dark"
                                                size="normal"
                                                ref={gReCaptchaRef}
                                                onChange={(value) => {
                                                    handleChange({target: {name: 'captcha', value}})
                                                }}/>
                                        </div>

                                        <button
                                            disabled={isSubmitting}
                                            className="send-btn"
                                            type="submit">Отправить
                                        </button>
                                    </StyledForm>
                                </FeedbackFormStyled>
                            )
                        }}
                    </Formik>

                    {portal(
                        <SuccessMessage onClick={() => {
                            onClosePopup(closePortal)
                        }}>
                            <div className="wrap">
                                <div className="close"><CrossIcon/></div>
                                <div className="title">Благодарим за ваше обращение!</div>
                                <div className="subtitle">Наши специалисты свяжутся с Вами в ближайшее время.
                                </div>
                            </div>
                        </SuccessMessage>
                    )}
                </>
            )}
        </PortalWithState>
    )
};

const FeedbackFormStyled = styled('div')`
    padding: 33px 32px 43px 26px;
    background: #091627;
    color: #fff;
    box-sizing: border-box;
    .section {
        &-title {
            margin-bottom: 23px;
            font-size: 28px;
            line-height: 130%;
            font-family: 'Circe-300', sans-serif;
            text-transform: uppercase;
            word-break: break-all;
        }
        &-subtitle {
            margin-bottom: 24px;
            font-size: 24px;
            line-height: 160%;
            letter-spacing: 0.05em;
            text-transform: uppercase;
        }
    }
    @media (min-width: 1024px) {
        padding: 92px 44px 70px 39px;
        display: flex;
        .section-title {
            margin-bottom: 49px;
        }
        .wrap {
            width: 33.33%;
        }
    }
    @media (min-width: 1280px) {
        padding: 90px 20px 69px 119px;
        grid-gap: 41px 23px;
        .section-title {
            margin-bottom: 34px;
            font-size: 34px;
        }
        .wrap {
            width: 30%;
        }
    }
    @media (min-width: 1600px) {
        padding: 90px 0 69px 156px;
        .section-title {
            margin-bottom: 25px;
            font-size: 54px;
        }
        .wrap {
            
        }
    }
`;

const StyledForm = styled('form')`
    display: grid;
    grid-template-areas: 
        "name"
        "organisation"
        "phone"
        "post"
        "email"
        "agreement"
        "captcha-wrap"
        "send-btn";
    .name {
        grid-area: name;
    }
    .organisation {
        grid-area: organisation;
    }
    .phone {
        grid-area: phone;
    }
    .post {
        grid-area: post;
    }
    .email {
        grid-area: email;
    }
    .agreement {
        grid-area: agreement;
    }
    .input {
        padding: 17px 0;
        margin-bottom: 33px;
        font-size: 21px;
        background-color: transparent;
        color: #fff;
        border: none;
        border-bottom: 1px solid #fff;
        border-radius: 0;
        box-sizing: border-box;
        &:focus {
            outline: none;
        }
        &::placeholder {
            color: #fff;
        }
        &_err {
            color: #FF0000;
            border-bottom: 1px solid #FF0000;
            &::placeholder {
                color: #FF0000;
            }
        }
    }
    .label {
        position: relative;
        padding-left: 33px;
        margin-bottom: 30px;
        margin-top: 3px;
        &__checkbox {
            display: none;
            &:checked + .label__icon {
                background-color: #fff;
            }
        }
        &__text {
            font-size: 21px;
            line-height: 120%;
            a {
               color: #fff;
            }
        }
        &__icon {
            position: absolute;
            top: 4px;
            left: 0;
            display: block;
            width: 15px;
            height: 15px;
            background-color: transparent;
            border: 1px solid #FFFFFF;
            border-radius: 1px;
            box-sizing: border-box;
            transition: background-color .3s;
        }
        &_err {
            .label__icon {
                border: 1px solid #FF0000;
            }
            .label__text, a {
                color: #FF0000;
            }
        }
    }
    .captcha-wrap {
        grid-area: captcha-wrap;
        transform: scale(1.02);
        margin: 0 auto;
    }
    .send-btn {
        grid-area: send-btn;
        justify-self: center;
        padding: 15px 72px;
        margin-top: 36px;
        font-size: 21px;
        line-height: 120%;
        letter-spacing: 0.2em;
        background-color: transparent;
        color: #fff;
        text-transform: uppercase;
        border: 1px solid #FFFFFF;
        box-sizing: border-box;
        transition: background-color .3s, color .3s;
        &:focus {
            outline: none;
        }
        &:disabled {
            opacity: .5;
            background-color: rgba(0,0,0,0.53);
        }
    }
    @media (min-width: 1024px) {
        padding-left: 15px;
        width: 66.66%;
        grid-gap: 46px 23px;
        grid-template-columns: 48%;
        grid-template-rows: auto;
        grid-template-areas: 
        "name name"
        "organisation post"
        "phone email"
        "agreement agreement"
        "captcha-wrap send-btn";
        box-sizing: border-box;
        .label {
            margin-bottom: 12px;
            &:hover {
                cursor: pointer;
            }
        }
        .send-btn {
            padding: 15px 60px;
            margin-top: 0;
            align-self: center;
            justify-self: flex-end;
            &:hover {
                background-color: #fff;
                color: #091627;
                cursor: pointer;
            }
            &:hover:disabled {
                background-color: rgba(0,0,0,0.53);
                color: #fff;
            }
        }
        .input {
            margin-bottom: 0;
            padding: 16px 0;
        }
        .captcha-wrap {
            justify-self: flex-start;
            margin: 0;
            transform: scale(0.77);
            transform-origin: left center;
        }
    }
    @media (min-width: 1280px) {
        grid-gap: 41px 43px;
        grid-template-columns: 47%;
        width: 70%;
        padding-left: 33px;
        padding-right: 86px;
        .label {
            margin-top: 15px;
            margin-bottom: 22px;
        }
        .send-btn {
            margin-left: auto;
            padding: 15px 58px;
        }
        .captcha-wrap {
           
        }
    }
    @media (min-width: 1600px) {
        grid-gap: 40px 61px;
        padding-left: 25px;
        padding-right: 164px;
    }
`;

const fadeIn = keyframes`
    0% {
        opacity: 0;
        transform: translate(-50px, 0);
    }
    100% {
        opacity: 1;
        transform: translate(0, 0);
    }
`;

const SuccessMessage = styled('div')`
    position: fixed;
    display: flex;
    align-items: center;
    justify-content: center;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.5);
    z-index: 1;
    .wrap {
        position: relative;
        padding: 50px 30px 60px 30px;
        width: 87%;
        max-width: 320px;
        margin: 0 auto;
        background: #F2F2F2;
        color: #091627;
        box-sizing: border-box;
        animation: .3s ${fadeIn} ease-out;
    }
    .title {
        margin-bottom: 20px;
        font-size: 24px;
        line-height: 130%;
        letter-spacing: 0.05em;
        text-transform: uppercase;
    }
    .subtitle {
        font-size: 21px;
        line-height: 120%;
    }
    .close {
        position: absolute;
        top: 15px;
        right: 15px;
    }
    @media (min-width: 1024px) {
        .wrap {
            max-width: 508px;
        }
        .close {
            top: 30px;
            right: 30px;
            &:hover {
                cursor: pointer;
            }
        }
    }
`;