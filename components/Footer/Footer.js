import React from 'react';
import CompanyLogoIcon from '../../svg/logo.svg';
import GetConsultationIcon from "../../svg/get-consultation.svg";
import PhoneIcon from "../../svg/phone.svg";
import PinkchilliLogoIcon from "../../svg/pinkchilli-logo.svg";
import scrollIntoView from "scroll-into-view";
import {listOfNavigationLinksFooter} from "../../fakeData/listOfNavigationLinks";
import {useRouter} from "next/router";
import {FooterStyled} from "./styled";

export const Footer = () => {
  const router = useRouter();

  const handleClickMenuItem = (e, anchorName, hashName) => {
    e.preventDefault();
    const target = document.querySelector((`div[data-id=${anchorName}]`));

    scrollIntoView(target, {
      time: 1000,
      align: {
        top: 0,
        left: 0,
        leftOffset: 0,
        topOffset: 0
      }
    }, () => {
      router.push(hashName)
    });
  };

  return (
    <FooterStyled>
      <div className="col">
        <div className="company-logo"><CompanyLogoIcon/></div>
        <div className="copyright">Все права защищены <br/> НТЦ ЕЭС Управление
          Энергоснабжением <br/> {new Date().getFullYear()} г.
        </div>
      </div>

      <div className="col">
        <nav className="navigation">
          <ul className="navigation__list">
            {
              listOfNavigationLinksFooter.map(item => {
                return (
                  <li key={item.anchorName}
                      className="item">
                    <a
                      onClick={(e) => handleClickMenuItem(e, item.anchorName, item.hashName)}
                      href={item.hashName}>{item.label}</a>
                  </li>
                )
              })
            }
          </ul>
        </nav>
      </div>

      <div className="col">
        <a
          onClick={(e) => handleClickMenuItem(e, 'form', '/#получить-консультацию')}
          className="get-consultation"
          href='/#получить-консультацию'>
          <GetConsultationIcon/>
          <div className="get-consultation__label">Получить консультацию</div>
        </a>

        <div className="divider"></div>

        <a href="tel:+74997881749" className="tel">
          <PhoneIcon/>
          <div className="tel__label">+7 (499) 788-17-49</div>
        </a>
      </div>

      <div className="col">
        <a href="https://pinkchilli.agency/" target="_blank" className="develop-by">
          <div className="develop-by__label">Разработка сайта:</div>
          <div className="develop-by__logo-wrap"><PinkchilliLogoIcon/></div>
        </a>
      </div>
    </FooterStyled>
  )
};