import styled from "styled-components";

export const FooterStyled = styled('footer')`
    padding: 37px 25px 27px 39px;
    display: grid;
    grid-template-columns: 62% 38%;
    grid-template-areas: 
    "col-1 col-3"
    "col-1 col-4";
    background: #091627;
    color: #fff;
    box-sizing: border-box;
    a {
        color: #fff;
        text-decoration: none;
    }
    .col {
        &:nth-child(1) {
            grid-area: col-1;
        }
        &:nth-child(2) {
            grid-area: col-2;
            display: none;
        }
        &:nth-child(3) {
            grid-area: col-3;
            position: relative;
            display: flex;
            align-self: self-start;
            justify-self: self-end;
            &::before {
                position: absolute;
                content: '';
                left: -23px;
                top: 0;
                display: block;
                width: 1px;
                height: 100%;
                background-color: #fff;
            }
        }
        &:nth-child(4) {
            grid-area: col-4;
            align-self: self-start;
            justify-self: self-end;
        }
    }
    .company-logo {
        margin-bottom: 20px;
        font-size: 12px;
        line-height: 120%;
    }
    .copyright {
        position: relative;
        padding-left: 18px;
        font-size: 12px;
        line-height: 120%;
        box-sizing: border-box;
        &::before {
            content: '©';
            font-size: 13px;
            display: block;
            position: absolute;
            top: 0;
            left: 0;
        }
    }
    .get-consultation {
        order: 3;
        &__label {
            display: none;
        }
        svg {
            width: 21px;
            height: 21px;
        }
    }
    .tel {
        order: 1;
        &__label {
            display: none;
        }
        svg {
            width: 21px;
            height: 21px;
        }
    }
    .divider {
        order: 2;
        margin: 0 30px;
        width: 1px;
        height: 25px;
        background-color: #fff;
    }
    .develop-by {
        margin-top: 46px;
        display: block;
        &__label {
            font-size: 10px;
            font-family: 'Circe-300';
        }
        svg {
            width: 77px;
            height: 36px;
        }
        
    }
    @media (min-width: 1024px) {
        padding: 52px 31px 30px 39px;
        grid-template-columns: 30% 30% 40%;
        grid-template-areas: 
            "col-1 col-2 col-3"
            "col-1 col-2 col-4";
        .col {
            &:nth-child(2) {
                display: block;
            }
            &:nth-child(3) {
                margin-top: 6px;
                margin-right: 8px;
                display: flex;
                flex-direction: column;
                align-items: flex-end;
                &::before {
                    display: none;
                }
            }
            &:nth-child(3) {
                margin-right: 8px;
            }
        }
        .copyright {
            font-size: 18px;
        }
        .navigation {
            margin-left: 40px;
            margin-top: -10px;
            &__list {
                display: flex;
                flex-direction: column;
                align-items: flex-start;
                .item {
                    display: inline-block;
                    padding: 9px 0; 
                    font-size: 18px;
                    line-height: 100%;
                    &:hover {
                        cursor: pointer;
                    }
                }
            }
        }
        .divider {
            display: none;
        }
        .get-consultation {
            display: flex;
            align-items: center;
            &__label {
                display: block;
                margin-left: 15px;
                font-size: 16px;
                text-transform: uppercase;
            }
            svg {
                width: 23px;
                height: auto;
            }
            &:hover {
                cursor: pointer;
            }
        }
        .tel {
            display: flex;
            align-items: center;
            margin-bottom: 19px;
            &__label {
                display: block;
                margin-left: 15px;
                font-size: 18px;
                text-transform: uppercase;
            }
            svg {
                width: 17px;
                height: 16px;
            }
            &:hover {
                cursor: pointer;
            }
        }
        .company-logo {
            margin-bottom: 27px;
        }
        .develop-by {
            margin-top: 52px;
            margin-right: 12px;
            &__logo-wrap svg {
                width: 93px;
                height: 51px;
            } 
        }
    }
    @media (min-width: 1280px) {
        padding: 52px 25px 22px 120px;
        grid-template-columns: 25% 25% 30% 20%;
        grid-template-areas:
        "col-1 col-2 col-3 col-4";
        .navigation {
            margin-left: 45px;
            margin-top: -7px;
        }
        .col:nth-child(3) {
            margin-right: 42px;
            margin-top: 4px;
        }
        .develop-by {
            margin-top: 4px;
            margin-right: 83px;
        }
        .tel svg {
            width: 17px;
            height: 17px;
        }
        .get-consultation svg {
            width: 17px;
            height: 17px;
        }
    }
    @media (min-width: 1600px) {
        padding: 58px 25px 54px 161px;
        .copyright {
            padding-left: 25px;
            font-size: 21px;
            &::before {
                font-size: 20px;
            }
        }
        .company-logo {
            margin-bottom: 15px;
        }
        .navigation {
            margin-left: 114px;
            &__list {
                .item {
                    padding: 6px 0;
                    font-size: 21px;
                }
            }
        }
        .col:nth-child(3) {
            margin-right: 72px;
        }
        .tel {
            margin-bottom: 24px;
        }
        .develop-by {
            margin-top: 0;
            margin-right: 135px;
            &__label {
                font-size: 14px;
            }
            &__logo-wrap svg {
                width: 110px;
                height: 65px;
            }
        }
    }
`;