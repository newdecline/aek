import styled from "styled-components";

export const StyledProjectSpecificationSection = styled('div')`
    padding: 24px 31px 34px 26px;
    display: flex;
    flex-direction: column;
    color: #091627;
    box-sizing: border-box;
    .section-title {
        margin-bottom: 21px;
        text-transform: uppercase;
        font-size: 28px;
        line-height: 130%;
        font-family: 'Circe-300', sans-serif;
        word-break: break-all;
    }
    .paragraph-wrap {
        &__item {
            margin-bottom: 24px;
            font-size: 18px;
            line-height: 120%;
            &:last-child {
                margin-bottom: 0;
            }
        }
    }
    @media (min-width: 1024px) {
        flex-direction: row;
        padding: 40px 31px 56px 39px;
        .section-title {
            width: 33.33%;
        }
        .paragraph-wrap {
            width: 66.66%;
            padding-left: 15px;
            &__item {
                margin-bottom: 29px;
                line-height: 123%;
            }
        }
    }
    @media (min-width: 1280px) {
        padding: 68px 31px 68px 119px;
        .section-title {
            width: 30%;
            margin-right: 32px;
            font-size: 34px;
        }
        .paragraph-wrap {
            width: 60%;
            padding-left: 0;
        }
        .paragraph-wrap__item {
            margin-bottom: 32px;
            font-size: 21px;
        }
    }
    @media (min-width: 1600px) {
        padding: 79px 31px 71px 157px;
        .section-title {
            font-size: 54px;
        }
        .paragraph-wrap__item {
            margin-bottom: 67px;
        }
    }
`;