import styled from "styled-components";

export const StyledAboutProductSection = styled('div')`
    display: flex;
    flex-direction: column;
    background: #091627;
    color: #fff;
    .img-wrap {
        position: relative;
        height: 338px;
        overflow: hidden;
        img {
            position: absolute;
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
    }
    .wrap {
        padding: 34px 34px 48px 26px;
        box-sizing: border-box;
    }
    .item {
        margin-bottom: 28px;
        font-size: 18px;
        line-height: 127%;
        &:last-child {
            margin-bottom: 0;
        }
    }
    .download {
      display: flex;
      align-items: center;
      font-size: 14px;
      line-height: 130%;
      letter-spacing: 0.05em;
      text-transform: uppercase;
      font-family: 'Circe-700', sans-serif;
      color: #fff;
      text-decoration: none;
      span {
        margin: 9px 0 0 10px;
      }
    }
    @media (min-width: 1024px) {
        flex-direction: row;
        .img-wrap {
            min-height: 100%;
            height: initial;
            width: 29%;
        }
        .item {
            margin-bottom: 31px;
        }
        .wrap {
            width: 71%;
            padding: 56px 34px 31px 36px;
        }
    }
    @media (min-width: 1280px) {
        .img-wrap {
            width: 37%;
        }
        .item {
            margin-bottom: 41px;
            font-size: 21px;
        }
        .wrap {
            padding: 84px 34px 49px 45px;
        }
    }
    @media (min-width: 1600px) {
        .wrap {
            padding: 113px 34px 79px 85px;
        }
        .img-wrap {
            width: 34%;
        }
    }
`;