import React from 'react';
import {StyledAboutProductSection} from "./styled";
import DownloadIcon from "../../../../svg/download.svg";

export const AboutProductSection = () => {
  return <StyledAboutProductSection id="о-продукте" data-id="about">
    <div className="img-wrap">
      <img src="/assets/images/about-section-img.jpg" alt="alt"/>
    </div>

    <div className="wrap">
      <p className="item">Организационно-правовая модель АЭК позволяет применять специальный механизм
        расчета объема оплаты услуг по передаче электрической энергии (по ставке, отражающей удельную
        величину расходов на содержание электрических сетей двухставочной цены (тарифа) для объектов
        потребителей, квалифицированных как объекты АЭК).</p>
      <p className="item">Одним из условий квалификации является управление электрическими режимами данных
        объектов ПАК УИС. </p>
      <p className="item">Помимо снижения объема оплаты услуг по передаче электрической энергии за счет
        квалификации объекта в качестве объекта АЭК, интеграция ПАК УИС приводит к снижению платы за
        электроэнергию и дополнительному снижению платы за мощность за счет автоматической экономической
        оптимизации купли-продажи электроэнергии и мощности в реальном времени. </p>
      <a target='_blank' href='/assets/presentation.pdf' className="download">
        <DownloadIcon/><span>Скачать презентацию</span>
      </a>
    </div>
  </StyledAboutProductSection>
};