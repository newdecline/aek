import React from 'react';
import PhoneIcon from "../../../../svg/phone.svg";
import MailIcon from "../../../../svg/mail.svg";
import LocationPinIcon from "../../../../svg/location-pin.svg";
import {MyMap} from "../../../Map";
import {StyledContactsSection} from "./styled";

export const ContactsSection = () => {
  return <StyledContactsSection data-id="contacts">
    <div className="wrap">
      <div className="section-title">Контакты</div>
      <div className="text-wrap">
        <div className="company-name">НТЦ ЕЭС Управление Энергоснабжением</div>

        <ul className="contacts-list">
          <li className="item">
            <div className="icon__wrap"><PhoneIcon/></div>
            <a href="tel:+74997881749" className="item__label">+7 (499) 788-17-49</a>
          </li>
          <li className="item">
            <div className="icon__wrap"><MailIcon/></div>
            <a href="mailto:info@активныйэнергокомплекс.рф"
               className="item__label">info@активныйэнергокомплекс.рф</a>
          </li>
          <li className="item">
            <div className="icon__wrap"><LocationPinIcon/></div>
            <div className="item__label">Россия, 109074, г. Москва, Китайгородский проезд, д. 7, стр.
              3
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div className="map-wrap">
      <MyMap/>
    </div>
  </StyledContactsSection>
};