import styled from "styled-components";

export const StyledContactsSection = styled('div')`
    display: flex;
    flex-direction: column;
    color: #091627;
    .wrap { 
        padding: 44px 16px 36px 26px;
        box-sizing: border-box;
    }
    .section-title {
        margin-bottom: 21px;
        font-size: 34px;
        font-family: 'Circe-300', sans-serif;
        text-transform: uppercase;
    }
    .company-name {
        margin-bottom: 29px;
        font-size: 14px;
    }
    .contacts-list {
        display: flex;
        flex-direction: column;
    }
    .item {
        margin-bottom: 14px;
        display: flex;
        align-items: start;
        font-size: 14px;
        line-height: 120%;
        a {
            color: #091627;
            text-decoration: none;
        }
        &:last-child {
            margin-bottom: 0;
            .icon__wrap {
                width: 15px;
            }
        }
    }
    .icon__wrap {
        margin-right: 15px;
        svg {
            width: 12px;
            path {
                fill: #000;
            }
        }
    }
    @media (min-width: 1024px) {
        .wrap {
            padding: 60px 31px 39px 39px;
            display: flex;
        }
        .company-name {
            margin-bottom: 24px;
            font-size: 18px;
        }
        .item {
            margin-bottom: 21px;
            font-size: 18px;
        }
        .section-title {
            width: 33.33%;
        }
        .text-wrap {
            width: 66.66%;
            padding-left: 15px;
        }
    }
    @media (min-width: 1280px) {
        .wrap {
            padding: 60px 86px 75px 119px;
        }
        .section-title {
            width: 30%;
        }
        .text-wrap {
            padding-left: 70px;
            width: 70%;
        }
        .item {
            margin-bottom: 25px;
            font-size: 21px;
        }
         .company-name {
            margin-bottom: 19px;
            font-size: 21px;
        }
        .icon__wrap {
            margin-right: 37px;
        }
    }
    @media (min-width: 1600px) {
        flex-direction: row;
        .wrap {
            width: 37%;
            flex-direction: column;
            padding: 87px 30px 75px 156px;
        }
        .map-wrap {
            width: 63%;
        }
        .text-wrap {
            padding-left: 0;
            width: initial;
        }
        .section-title {
            margin-bottom: 36px;
            font-size: 54px;
        }
        .company-name {
            margin-bottom: 27px;
        }
        .icon__wrap {
            margin-right: 25px;
            width: initial;
        }
        .item:last-child {
            svg {
                width: 17px;
                height: 17px;
            }
        }
    }
`;