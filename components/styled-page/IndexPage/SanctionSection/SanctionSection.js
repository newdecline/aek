import React from 'react';
import {StyledSanctionSection} from "./styled";

export const SanctionSection = () => {
  return <StyledSanctionSection id='постановление' data-id="act">
    <h6 className="section-title">Постановление <br/> правительства</h6>

    <div className="paragraph-wrap">
      <p className="paragraph-wrap__item">Постановление Правительства Российской Федерации от 21.03.2020 №&nbsp;320 "О
        внесении изменений в некоторые акты Правительства Российской Федерации по вопросам функционирования активных
        энергетических комплексов.</p>
      <div><a
        target='_blank'
        rel='noreferrer noopener'
        href="http://publication.pravo.gov.ru/Document/View/0001202003240012" className='link'>Посмотреть
        постановление</a></div>
      <div><a
        target='_blank'
        rel='noreferrer noopener'
        href="/assets/reference.pdf" className='link'>Справка</a></div>
      <div><a
        target='_blank'
        rel='noreferrer noopener'
        href="/assets/concept.pdf" className='link'>Концепция АЭК</a></div>
    </div>
  </StyledSanctionSection>
};