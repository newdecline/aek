import React from 'react';
import IndustrialSitesIcon from "../../../../svg/industrial-sites.svg";
import IndustrialParksIcon from "../../../../svg/industrial-parks.svg";
import EnergySalesOrganizationsIcon from "../../../../svg/energy-sales-organizations.svg";
import TTPRetailMarketIcon from "../../../../svg/ttp-retail-market.svg";
import StrategicGenerationInvestorsIcon from "../../../../svg/strategic-generation-investors.svg";
import EngineeringCompaniesIcon from "../../../../svg/engineering-companies.svg";
import {StyledForWhomSection} from "./styled";

export const ForWhomSection = () => {
  return <StyledForWhomSection id="для-кого" data-id="for-whom">
    <h6 className="section-title">Для кого</h6>

    <div className="wrap">
      <div className="item">
        <div className="item__icon-wrap"><IndustrialSitesIcon/></div>
        <div className="item__title">Промышленные <br/> площадки</div>
        <div className="item__subtitle">Владельцы производств, нацеленные на снижение затрат на
          энергоснабжение технологических процессов.
        </div>
      </div>
      <div className="item">
        <div className="item__icon-wrap"><IndustrialParksIcon/></div>
        <div className="item__title">Индустриальные <br/> парки</div>
        <div className="item__subtitle">Управляющие компании, заинтересованные в обеспечении конкурентных
          цен на электроэнергию и мощность для резидентов и в формировании дополнительного источника
          дохода.
        </div>
      </div>
      <div className="item">
        <div className="item__icon-wrap"><EnergySalesOrganizationsIcon/></div>
        <div className="item__title">Энергосбытовые <br/> организации</div>
        <div className="item__subtitle">Энергосбытовые организации, в операционных зонах которых находятся
          электростанции розничного рынка, рассматривающие варианты формирования дополнительного дохода.
        </div>
      </div>
      <div className="item">
        <div className="item__icon-wrap"><TTPRetailMarketIcon/></div>
        <div className="item__title">ТЭЦ розничного <br/> рынка</div>
        <div className="item__subtitle">Владельцы или управляющие компании ТЭЦ розничного рынка,
          заинтересованные в увеличении КИУМ.
        </div>
      </div>
      <div className="item">
        <div className="item__icon-wrap"><StrategicGenerationInvestorsIcon/></div>
        <div className="item__title">Стратегические инвесторы в генерацию</div>
        <div className="item__subtitle">Компании, инвестирующие в электростанции розничного рынка.</div>
      </div>
      <div className="item">
        <div className="item__icon-wrap"><EngineeringCompaniesIcon/></div>
        <div className="item__title">Инжиниринговые <br/> компании</div>
        <div className="item__subtitle">Компании, разрабатывающие и реализующие проекты энергетических
          инфраструктур.
        </div>
      </div>
    </div>
  </StyledForWhomSection>
};