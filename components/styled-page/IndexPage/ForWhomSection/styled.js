import styled from "styled-components";

export const StyledForWhomSection = styled('div')`
    padding: 29px 31px 49px 26px;
    display: flex;
    flex-direction: column;
    background: #f2f2f2;
    color: #091627;
    box-sizing: border-box;
    .section-title {
        margin-bottom: 31px;
        text-transform: uppercase;
        font-size: 28px;
        font-family: 'Circe-300';
    }
    .item {
        margin-bottom: 40px;
        &:last-child {
            margin-bottom: 0;
        }
        &__icon-wrap {
            margin-bottom: 21px;
        }
        &__title {
            margin-bottom: 11px;
            font-size: 28px;
            font-family: 'Circe-700';
        }
        &__subtitle {
            font-size: 18px;
            line-height: 120%;
        }
    }
    @media (min-width: 1024px) {
        padding: 40px 31px 49px 39px;
        .wrap {
            display: grid;
            grid-template-columns: 1fr 1fr 1fr;
            grid-gap: 23px 29px;
        }
        .section-title {
            margin-bottom: 39px;
        }
        .item {
            margin-bottom: 0;
            &__title {
                margin-bottom: 15px;
            }
        }
    }
    @media (min-width: 1280px) {
        padding: 60px 31px 49px 119px;
        .section-title {
            margin-bottom: 39px;
            font-size: 34px;
        }
        .wrap {
            grid-template-columns: 30% 30% 30%;
        }
        .item {
            &__title {
                margin-bottom: 25px;
                font-size: 34px;
            }
            &__subtitle {
                font-size: 21px;
            }
        }
    }
    @media (min-width: 1600px) {
        padding: 90px 31px 91px 156px;
        .section-title {
            font-size: 54px;
        }
        .item {
            &__title {
                font-size: 36px;
            }
            &__icon-wrap {
               margin-bottom: 30px; 
            }
        }
    }
`;