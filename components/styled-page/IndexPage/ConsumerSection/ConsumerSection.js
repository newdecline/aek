import React from 'react';
import {StyledConsumerSection} from "./styled";

export const ConsumerSection = () => {
  return <StyledConsumerSection>
    <div className="wrap">
      <picture className="picture">
        <source
          media="(min-width: 1024px)"
          srcSet="/assets/svg/consumption-pattern-big.svg"/>
        <img
          src="/assets/svg/consumption-pattern-small.svg" alt=""/>
      </picture>
    </div>
  </StyledConsumerSection>
};