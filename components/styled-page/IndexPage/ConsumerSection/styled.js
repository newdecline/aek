import styled from "styled-components";

export const StyledConsumerSection = styled('div')`
    position: relative;
    padding-bottom: 26px;
    background: #F2F2F2; 
    box-sizing: border-box;
    z-index: 1;
    .wrap {
        position: relative;
        height: 308px;
        left: calc(50% - 3.5px);
        display: flex;
        justify-content: center;
        transform: translate(-50%, 0);
        box-sizing: border-box;
        .picture {
            position: absolute;
        }
    }
    @media (min-width: 1024px) {
        //top: 0;
        padding-bottom: 60px;
        overflow-x: hidden;
        .wrap {
            height: 686px;
            left: calc(50% + 1px);
            transform: translate(-50%, -1px);
        }
    }
    @media (min-width: 1600px) {
        padding-bottom: 91px;
        .wrap {
            height: 686px;
        }
    }
`;