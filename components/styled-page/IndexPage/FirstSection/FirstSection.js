import React from 'react';
import JetIcon from "../../../../svg/jet.svg";
import {StyledFirstSection} from "./styled";

export const FirstSection = () => {
  return <StyledFirstSection data-id="main">
    <div className="wrap">
      <div className="company-name">
        <div className="company-name__item">
          <div>А</div>
        </div>
        <div className="company-name__item">
          <div>Э</div>
        </div>
        <div className="company-name__item">
          <div>К</div>
        </div>
      </div>

      <div className="content-bottom">
        <div className="col">
          <div className="col-item">
            <div className="col-item__title">Новая</div>
            <div className="col-item__subtitle">организационно-правовая модель энергоснабжения
              промышленных потребителей
            </div>
          </div>
          <div className="col-item">
            <div className="col-item__title">Новая</div>
            <div className="col-item__subtitle">технология взаимодействия генерации и промышленных
              потребителей
            </div>
          </div>
        </div>
        <div className="col">
          <h6 className="col__title">Снижение платы <br/> за&nbsp;электроэнергию и мощность для&nbsp;промышленных
            потребителей</h6>
          <p className="col__subtitle">подключенных к шинам электростанции, за счет перехода на новую
            организационно-правовую модель активного энергетического комплекса (АЭК) и внедрения
            программно-аппаратного комплекса управляемого интеллектуального соединения (ПАК
            УИС).</p>
        </div>
      </div>
    </div>

    <div className="section-border">
      <div className="title">Сеть общего пользования</div>
      <div className="icon-wrap"><JetIcon/></div>
    </div>
  </StyledFirstSection>
};