import styled from "styled-components";

export const StyledFirstSection = styled('div')`
    position: relative;
    padding-bottom: 103px;
    background: url("/assets/images/first-section-bg.jpg") no-repeat center -262px;
    color: #fff;
    overflow-x: hidden;
    box-sizing: border-box;
    .wrap {
        overflow-x: hidden;
        margin-bottom: 12px;
    }
    .company-name {
        display: flex;
        padding: 0 0 0 26px;
        margin-right: -44px;
        box-sizing: border-box;
        &__item {
            display: flex;
            width: 100%;
            padding-top: 146px;
            border-left: 1px solid #96C2EB;
            font-size: 195px;
            font-family: 'Circe-800';
            line-height: 103px;
            overflow: hidden;
            box-sizing: border-box;
            &:nth-child(1) {
                div {
                    transform: translate(-27px, 0);
                }
            }
            &:nth-child(2) {
                div {
                    transform: translate(-24px, 0);
                }
            }
            &:nth-child(3) {
                div {
                    transform: translate(-22px, 0);
                } 
            }
        }
    }
    .content-bottom {
        display: flex;
        flex-direction: column;
        padding: 52px 22px 0px 26px;
        background: linear-gradient(180deg, rgba(8, 18, 30, 0.3) 6.25%, rgba(9, 22, 39, 0) 100%);
        box-sizing: border-box;
    }
    .col {
        &:nth-child(1) {
            order: 2;
        }
        &:nth-child(2) {
            order: 1;
        }
    }
    .col__title {
        margin-bottom: 18px;
        color: #96C2EB;
        font-size: 28px;
        line-height: 120%;
        font-family: 'Circe-300';
        text-transform: uppercase;
    }
    .col__subtitle {
        margin-bottom: 62px;
        font-size: 18px;
    }
    .col-item {
        margin-bottom: 24px;
        &__title {
            font-size: 24px;
            letter-spacing: 0.05em;
            text-transform: uppercase;
        }
        &__subtitle {
            font-size: 18px;
             letter-spacing: 0.05em;
        }
    }
    .section-border {
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 103px;
        display: flex;
        flex-direction: column;
        align-items: center;
        box-sizing: border-box;
        z-index: 1;
        .title {
            margin-bottom: 15px;
            font-family: 'Circe-800';
            font-size: 14px;
            text-transform: uppercase;
            text-align: center;
        }
        .icon-wrap {
            transform: translate(9px, 0);
        }
        svg {
            width: 22px;
            height: 40px;
        }
    }
    &::after {
        content: '';
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 103px;
        background: transparent url("/assets/svg/section-border-small.svg") no-repeat center bottom;
    }
    @media (min-width: 1024px) {
        padding-bottom: 121px;
        background-size: 115% 115%;
        background-position: center -100px;
        .company-name {
            width: initial;
            padding: 0 0 0 38px;
            margin-right: 0;
            &__item {
                margin-right: 10%;
                padding-right: 0;
                padding-top: 193px;
            }
        }
        .content-bottom {
            padding: 74px 22px 0 38px;
            flex-direction: row;
        }
        .col {
            &:nth-child(1) {
                order: 1;
                width: 33.3%;
                margin-right: 19px;
            }
            &:nth-child(2) {
                order: 2;
                width: 66.6%;
            }
        }
        .col-item {
            margin-bottom: 37px;
        }
        .col__title {
            margin-bottom: 27px;
        }
        .section-border {
            padding-top: 51px;
            height: 138px;
            .title {
                margin-bottom: 11px;
            }
            .icon-wrap {
                transform: translate(7px,0);
            }
        }
        &::after {
            background-size: 100% 100%;
        }
    }
    @media (min-width: 1280px) {
        padding-bottom: 138px;
        .company-name {
            padding: 0 0 0 119px;
            margin-right: 58px;
        }
        .content-bottom {
            padding: 62px 22px 0 119px;
        }
        .col-item {
            margin-bottom: 33px;
            &__title {
                margin: 5px 0;
            }
            &__subtitle {
                font-size: 21px;
            }
        }
        .col:nth-child(1) {
            width: 32%;
            margin-right: 0;
            padding-right: 15px;
            box-sizing: border-box;
        }
        .col:nth-child(2) {
            width: 66.66%;
        }
        .col__title {
            margin-bottom: 14px;
            font-size: 34px;
            line-height: 128%;
        }
        .col__subtitle {
            width: 73%;
            font-size: 21px;
        }
        .section-border {
            padding-top: 19px;
            .title {
                margin-bottom: 16px;
            }
            svg {
                width: 29px;
                height: 52px;
            }
        }
    }
    @media (min-width: 1600px) {
        padding-bottom: 174px;
        background-position: 89% -166px;
        .wrap {
            min-height: calc(var(--vh, 1vh) * 100);
        }
        .company-name {
            padding: 0 0 0 158px;
            margin-right: 117px;
            &__item {
                font-size: 300px;
                padding-top: 289px;
                line-height: 148px;
                &:nth-child(1) {
                    div {
                        transform: translate(-45px, 0);
                    }
                }
                &:nth-child(3) {
                    div {
                        transform: translate(-39px, 0);
                    }
                }
            }
        }
        .col__title {
            margin-bottom: 35px;
            font-size: 54px;
            line-height: 130%;
        }
        .col__subtitle {
            line-height: 111%;
        }
        .col-item__title {
            margin: 3px 0;
        }
        .content-bottom {
            padding: 100px 22px 0 161px;
        }
        .col:nth-child(1) {
            width: 23%;
            margin-top: 12px;
            margin-right: 143px;
        }
        .section-border {
            height: 193px;
            padding-top: 48px;
            .title {
                margin-bottom: 16px;
            }
            svg {
                width: 29px;
                height: 52px;
            }
        }
        &::after {
            height: 186px;
        }
    }
    @media (min-width: 1920px) {
        background-size: 100%;
    }
`;