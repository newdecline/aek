import React, {useEffect} from 'react';
import styled from "styled-components";
import Link from "next/link";
import {useSelector, useDispatch} from "react-redux";
import {enablePageScroll, disablePageScroll} from 'scroll-lock';
import {togglePageMenu} from "../redux/actions/actionCreators";
import LogoIcon from "../svg/logo.svg";
import GetConsultationIcon from "../svg/get-consultation.svg";
import PhoneIcon from "../svg/phone.svg";
import {listOfNavigationLinksHeader} from "../fakeData/listOfNavigationLinks";

export const PageErrorHeader = () => {
    const {isOpenMenu} = useSelector(({app}) => app);

    const dispatch = useDispatch();

    const handleClickBurgerBtn = () => {
        dispatch(togglePageMenu(!isOpenMenu));
    };

    const handleClickMenuItem = () => {
        dispatch(togglePageMenu(false));
    };

    useEffect(() => {
        if (isOpenMenu) {
            disablePageScroll();
        } else {
            enablePageScroll();
        }
    });

    return (
        <HeaderStyled open={isOpenMenu}>
            <BurgerMenu open={isOpenMenu} onClick={handleClickBurgerBtn}>
                <span className='item'></span>
                <span className='item'></span>
                <span className='item'></span>
            </BurgerMenu>

            <div className="logo-wrap">
                <Link href="/"><a><LogoIcon/></a></Link>
            </div>

            <nav className="navigation">
                <ul className='navigation__list'>
                    {
                        listOfNavigationLinksHeader.map(item => (
                            <li
                                key={item.anchorName}
                                className="navigation__item">
                                <a
                                    href={item.hashName}
                                    onClick={handleClickMenuItem}
                                    className="navigation__item-link">{item.label}</a>
                            </li>
                        ))
                    }
                </ul>
            </nav>

            <div className="wrap">
                <a href='/#получить-консультацию' className="get-consultation">
                    <GetConsultationIcon/>
                    <div className="get-consultation__label">Получить консультацию</div>
                </a>
                <div className="divider"></div>
                <a href="tel:+74997881749" className="tel">
                    <PhoneIcon/>
                    <div className="tel__label">+7 (499) 788-17-49</div>
                </a>
            </div>
        </HeaderStyled>
    )
};

const HeaderStyled = styled('header')`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    display: flex;
    align-items: center;
    padding: 27px 27px 27px 43px;
    z-index: 1;
    box-sizing: border-box;
    .navigation {
        position: fixed;
        top: 0;
        right: 0;
        padding: 79px 0 0 43px;
        width: 100%;
        height: 100%;
        display: block;
        transform: ${({open}) => open ? 'translate3d(0, 0, 0)' : 'translate3d(100%, 0, 0)'};
        background-color: #091627;
        color: #fff;
        transition: transform .3s;
        box-sizing: border-box;
        &__item {
            margin-bottom: 22px;
            &:last-child {
                margin-bottom: 0;
            }
        }
        &__item-link {
            display: inline-block;
            padding: 5px 0;
            letter-spacing: 0.05em;
            text-transform: uppercase;
            font-size: 14px;
            font-family: 'Circe-700';
            color: #fff;
            text-decoration: none;
        }
    }
    .logo-wrap {
        flex: 1;
        z-index: 1;
        svg {
            width: 97px;
            height: 28px;
        }
    }
    .get-consultation {
        z-index: 1;
        &__label {
            display: none;
        }
        svg {
            width: 22px;
            height: 20px;
        }
    }
    .divider {
        margin: 0 23px;
        height: 22px;
        width: 1px;
        background-color: #96C2EB;
        z-index: 1;
    }
    .tel {
        z-index: 1;
        &__label {
            display: none;
        }
    }
    .wrap {
        flex: 1.3;
        display: flex;
    }
    @media (min-width: 1024px) {
        padding: 41px 27px 27px 55px;
        .logo-wrap {
            width: initial;
            height: initial;
            margin-right: 31px;
            svg {
                width: 165px;
                height: 47px;
            }
        }
        .navigation {
            position: static;
            transform: initial;
            padding: 0;
            width: initial;
            background: transparent;
            &__list {
                display: flex;
                flex-direction: column;
            }
            &__item {
                margin-bottom: 4px;
                &:first-child {
                    display: none;
                }
                &:hover {
                    cursor: pointer;
                }
                &-link {
                    padding: 2px 0;
                }
            }
        }
        .divider {
            display: none;
        }
        .get-consultation, .tel {
            display: flex;
            align-items: center;
            color: #fff;
            text-decoration: none;
        }
        .get-consultation {
            order: 2;
            font-size: 14px;
            font-family: 'Circe-700';
            text-transform: uppercase;
            &__label {
                margin-left: 10px;
            }
            svg {
                transform: translate(0, -2px);
            }
        }
        .tel {
            order: 1;
            margin-bottom: 8px;
            font-size: 16px;
            font-family: 'Circe-700';
            text-transform: uppercase;
            &__label {
                margin-left: 10px;
            }
            svg {
                transform: translate(0, -2px);
            }
        }
        .get-consultation__label, .tel__label {
            display: block;
        }
        .wrap {
            margin-left: auto;
            flex-direction: column;
            align-items: flex-end;
            align-self: flex-start;
        }
    }
    @media (min-width: 1280px) {
        padding: 41px 103px 27px 181px;
        .logo-wrap {
            margin-right: 0;
            flex: 0.9;
        }
    }
    @media (min-width: 1600px) {
        .navigation {
            margin-left: 15px;
            &__list {
                flex-direction: row;
            }
            &__item {
                margin-right: 24px;
                &-link {
                    font-size: 16px;
                }
            }
        }
        .logo-wrap {
            flex: 1.4;
        }
        .wrap {
            flex-direction: row;
            align-self: center;
            margin: -1px 58px 0 0;
            justify-content: flex-end;
        }
        .tel {
            margin-bottom: 0;
            order: 2;
            &__label {
                margin-left: 9px;
                font-size: 18px;
            }
        }
        .get-consultation {
            order: 1;
            margin-right: 34px;
            &__label {
                margin-left: 10px;
                font-size: 16px;
            }
        }
    }
`;

const BurgerMenu = styled('div')`
    position: absolute;
    top: 27px;
    right: 31px;
    display: inline-flex;
    flex-direction: column;
    justify-content: space-between;
    width: 29px;
    z-index: 1;
    .item {
        display: block;
        width: 100%;
        height: 4px;
        margin-bottom: 6px;
        background: #E5E5E5;
        transform-origin: center;
        transition: width .3s, transform .3s;
        &:first-child {
            transform: ${({open}) => open ? 'rotate(45deg) translate3d(6px, 8px, 0)' : 'rotate(0)'};
        }
        &:nth-child(2) {
            width: ${({open}) => open ? '0%' : '100%'};
        }
        &:last-child {
            transform: ${({open}) => open ? 'rotate(-45deg) translate3d(7px, -8px, 0)' : 'rotate(0)'};
            margin-bottom: 0;
        }
    }
    @media (min-width: 1024px) {
        display: none;
    }
`;