import React from "react";
import {Header} from "../Header/Header";
import {Footer} from "../Footer/Footer";
import Head from "next/head";

const isProduction = process.env.NODE_ENV === 'production';

const ymJs = `
(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

ym(${process.env.YMID}, "init", {
    clickmap:true,
    trackLinks:true,
    accurateTrackBounce:true,
    webvisor:true
});
`;

export const MainLayout = props => {
    const {
        children,
    } = props;

    return (
        <>
            <Head>
                <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
                <link rel="icon" type="image/x-icon" href="/assets/favicon.ico"/>
                <title>Активный энергокомплекс - НТЦ ЕЭС</title>
                {(isProduction) && <>
                    <script dangerouslySetInnerHTML={{__html: ymJs}}></script>
                    <noscript>
                        <div><img src={`https://mc.yandex.ru/watch/${process.env.YMID}`} style={{position: 'absolute', left: -9999}} alt=""/></div>
                    </noscript>
                </>}
            </Head>

            <Header/>

            {children}

            <Footer/>
        </>
    )
};