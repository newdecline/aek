import React from "react";
import {PageErrorHeader} from "../PageErrorHeader";
import Head from "next/head";

export const PageErrorLayout = props => {
    const {
        children,
    } = props;

    return (
        <>
            <Head>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <link rel="icon" type="image/x-icon" href="/assets/favicon.ico" />
                <title>Активный энергокомплекс - НТЦ ЕЭС</title>
            </Head>

            <PageErrorHeader />

            {children}
        </>
    )
};