import React, {useState, useEffect} from 'react';
import { YMaps, Map, Placemark  } from 'react-yandex-maps';
import { uuid } from 'uuidv4';

export const MyMap = () => {
    // let map;
    const [mapData, setMapData] = useState({
        center: [55.75142390645039,37.63533350436912],
        zoom: 17
    });
    const [fakeIdMap, setFakeIdMap] = useState(uuid());
    const [srcLocationPin, setSrcLocationPin] = useState('/assets/svg/location-pin-small.svg');

    // const onInstanceRef = ref => {
    //     if (ref !== null) {
    //         map = ref;
    //     }
    // };

    const handleFitToViewport = () => {
        if (window.matchMedia( "(min-width: 1024px)" ).matches) {
            setSrcLocationPin('/assets/svg/location-pin.svg');
        }

        setFakeIdMap(uuid());
    };

    useEffect(() => {
        window.addEventListener('resize', handleFitToViewport);
        return () => {
            window.removeEventListener('resize', handleFitToViewport);
        }
    }, []);

    return (
        <div className="map">
            <YMaps key={fakeIdMap}>
                <Map
                    defaultState={mapData}
                    // instanceRef={onInstanceRef}
                    width='100%'
                    height='100%'
                >
                    <Placemark
                        geometry={mapData.center}
                        options={{
                            iconLayout: 'default#image',
                            iconImageHref: srcLocationPin,
                            iconImageSize: [42, 57],
                            iconImageOffset: [-21, -48]
                        }}
                    />
                </Map>
            </YMaps>
        </div>
    )
};