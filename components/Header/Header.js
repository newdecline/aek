import React, {useEffect} from 'react';
import {useRouter} from 'next/router';
import {enablePageScroll, disablePageScroll} from 'scroll-lock';
import scrollIntoView from "scroll-into-view";
import {useSelector, useDispatch} from "react-redux";
import {togglePageMenu} from "../../redux/actions/actionCreators";
import {listOfNavigationLinksHeader} from "../../fakeData/listOfNavigationLinks";
import LogoIcon from "../../svg/logo.svg";
import GetConsultationIcon from "../../svg/get-consultation.svg";
import PhoneIcon from "../../svg/phone.svg";
import DownloadIcon from "../../svg/download.svg";
import {BurgerMenu, HeaderStyled} from "./styled";

export const Header = () => {
  const {isOpenMenu} = useSelector(({app}) => app);
  useEffect(() => {
    if (isOpenMenu) {
      disablePageScroll();
    } else {
      enablePageScroll();
    }
  });

  const dispatch = useDispatch();
  const router = useRouter();

  const handleClickBurgerBtn = () => {
    dispatch(togglePageMenu(!isOpenMenu));
  };

  const handleClickMenuItem = (e, anchorName, hashName) => {
    e.preventDefault();
    dispatch(togglePageMenu(false));

    const target = document.querySelector((`div[data-id=${anchorName}]`));

    scrollIntoView(target, {
      time: 1000,
      align: {
        top: 0,
        left: 0,
        leftOffset: 0,
        topOffset: 0
      }
    }, () => {
      router.push(hashName)
    });
  };

  return (
    <HeaderStyled open={isOpenMenu}>
      <BurgerMenu open={isOpenMenu} onClick={handleClickBurgerBtn}>
        <span className='item'/>
        <span className='item'/>
        <span className='item'/>
      </BurgerMenu>

      <div className="logo-wrap">
        <LogoIcon/>

        <div className="navigation__item presentation">
          <a
            target="_blank"
            href='/assets/presentation.pdf'
            className="navigation__item-link"><DownloadIcon/><span>ПРЕЗЕНТАЦИЯ</span></a>
        </div>
      </div>

      <nav className="navigation">
        <ul className='navigation__list'>
          {
            listOfNavigationLinksHeader.map(item => (
              <li
                key={item.anchorName}
                className="navigation__item">
                <a
                  onClick={(e) => handleClickMenuItem(e, item.anchorName, item.hashName)}
                  href={item.hashName}
                  className="navigation__item-link">{item.label}</a>
              </li>
            ))
          }
          <li className="navigation__item presentation">
            <a
              target="_blank"
              href='/assets/presentation.pdf'
              className="navigation__item-link"><DownloadIcon/><span>ПРЕЗЕНТАЦИЯ</span></a>
          </li>
        </ul>
      </nav>

      <div className="wrap">
        <div className="wrap__inner">
          <a
            onClick={(e) => handleClickMenuItem(e, 'form', '/#получить-консультацию')}
            className="get-consultation"
            href='/#получить-консультацию'>
            <GetConsultationIcon/></a>
          <div className="divider"/>
          <a href="tel:+74997881749" className="tel">
            <PhoneIcon/>
            <div className="tel__label">+7 (499) 788-17-49</div>
          </a>
        </div>
        <div className="navigation__item presentation">
          <a target="_blank" href='/assets/presentation.pdf' className="navigation__item-link">
            <DownloadIcon/><span>ПРЕЗЕНТАЦИЯ</span>
          </a>
        </div>
      </div>
    </HeaderStyled>
  )
};