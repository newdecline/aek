import styled from "styled-components";

export const HeaderStyled = styled('header')`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  display: flex;
  align-items: center;
  padding: 27px 27px 27px 43px;
  z-index: 1;
  box-sizing: border-box;
  a {
    text-decoration: none;
    color: inherit;
  }
  .navigation {
    position: fixed;
    top: 0;
    right: 0;
    padding: 79px 0 0 43px;
    width: 100%;
    height: 100%;
    display: block;
    transform: ${({open}) => open ? 'translate3d(0, 0, 0)' : 'translate3d(100%, 0, 0)'};
    background-color: #091627;
    color: #fff;
    transition: transform .3s;
    box-sizing: border-box;
    &__item {
      margin-bottom: 22px;
      &:last-child {
          margin-bottom: 0;
      }
      &.brochure {
        display: inline-flex;
        .navigation__item-link {
          display: inline-flex;
          align-items: center;
          span {
            margin-left: 10px;
          }
        }
      }
    }
    &__item-link {
      display: inline-block;
      padding: 5px 0;
      letter-spacing: 0.05em;
      text-transform: uppercase;
      font-size: 14px;
      font-family: 'Circe-700', sans-serif;
    }
  }
  .logo-wrap {
    flex: 1;
    z-index: 1;
    svg {
      display: block;
      width: 97px;
      height: 28px;
    }
    .presentation {
      display: none;
    }
  }
  .get-consultation {
      order: 3;
      z-index: 1;
      &__label {
          display: none;
      }
      svg {
          width: 22px;
          height: 20px;
      }
  }
  .divider {
      margin: 0 23px;
      height: 22px;
      width: 1px;
      background-color: #96C2EB;
      order: 2;
      z-index: 1;
  }
  .tel {
    order: 1;
    z-index: 1;
    &__label {
      display: none;
      font-size: 14px;
      line-height: 130%;
    }
  }
  .wrap {
    flex: 1.2;
    .presentation {
      display: none;
    }
  }
  .presentation span {
    margin-left: 10px;
  }
  .wrap__inner {
    display: flex;
  }
  @media (min-width: 1024px) {
    padding: 41px 41px 27px 55px;
    .logo-wrap {
      display: flex;
      flex: 1.37;
      margin-right: 0;
      width: initial;
      height: initial;
      svg {
        width: 165px;
        height: 47px;
      }
      .presentation {
        display: inline-flex;
        margin-left: 30%;
        .navigation__item-link {
          display: flex;
          align-items: center;
        }
        span {
          color: #fff;
        }
        svg {
          display: flex;
          width: 24px;
          height: auto;
        }
      }
    }
    .navigation {
      padding: 137px 0 0 55px;
      &__list {
        display: flex;
        flex-direction: column;
        .presentation {
          display: none;
        }
      }
      &__item {
        &-link {
          padding: 2px 0;
          &:hover {
            cursor: pointer;
          }
        }
      }
    }
    //.brochure.without-menu {
    //  display: inline-block;
    //  color: #fff;
    //  box-sizing: border-box;
    //}
    .get-consultation, .tel {
      display: flex;
      align-items: center;
      color: #fff;
      text-decoration: none;
      &:hover {
        cursor: pointer;
      }
    }
    .get-consultation {
      order: 3;
      font-size: 14px;
      font-family: 'Circe-700', sans-serif;
      text-transform: uppercase;
      &__label {
        margin-left: 10px;
      }
      svg {
        transform: translate(0, -2px);
      }
    }
    .tel {
      padding-left: 28px;
      order: 1;
      font-size: 16px;
      font-family: 'Circe-700', sans-serif;
      text-transform: uppercase;
      box-sizing: border-box;
      &__label {
        margin-left: 10px;
      }
      svg {
        transform: translate(0, -2px);
      }
    }
    .get-consultation__label, .tel__label {
      display: block;
    }
    .wrap {
      margin-left: auto;
      flex: 0.63;
    }
  }
  @media (min-width: 1280px) {
    padding: 41px 103px 27px 181px;
    .logo-wrap {
      flex: 1;
      .presentation {
        display: none;
      }
    }
    .navigation {
      flex: 1;
      position: static;
      transform: initial;
      padding: 0;
      width: initial;
      background: transparent;
      &__list {
        display: grid;
        grid-template-columns: auto auto;
        grid-template-rows: 1fr 1fr;
        grid-auto-flow: column;
      }
      &__item {
        margin-bottom: 0;
        &:first-child {
          display: none;
        }
        &.brochure {
          display: none;
        }
      }
      &__item-link {
        margin-bottom: 6px;
        font-size: 12px;
        line-height: 130%;
      }
    }
    .wrap {
      flex: 1;
      .presentation {
        display: flex;
        justify-content: flex-end;
        .navigation__item-link {
          margin-bottom: 0;
          display: flex;
          align-items: flex-end;
        }
        span {
          color: #fff;
        }
      }
    }
    .wrap__inner {
      justify-content: flex-end;
      margin-bottom: 11px;
    }
  }
  @media (min-width: 1600px) {
    padding: 44px 103px 27px 181px;
    .logo-wrap {
      flex: 0.5;
    }
    .navigation {
      margin-left: 15px;
      flex: 1.5;
      &__list {
        display: flex;
        flex-direction: row;
      }
      &__item {
        margin-right: 20px;
        margin-bottom: 0;
        &:nth-child(2) {
          margin-right: 36px;
        }
        &-link {
          margin-bottom: 0;
          font-size: 14px;
          line-height: 130%;
          letter-spacing: 0.05em;
        }
      }
    }
    .wrap {
      flex: 1;
      display: flex;
      align-items: baseline;
      justify-content: flex-end;
      .presentation {
        margin-right: 0;
      }
    }
    .wrap__inner {
      display: inline-flex;
      order: 2;
      margin-bottom: 0;
    }
    .tel {
      margin-bottom: 0;
      &__label {
        margin-left: 9px;
        font-size: 18px;
      }
      svg {
        transform: translate(0,0px);
      }
    }
    .get-consultation {
      margin-right: 0;
      &__label {
        margin-left: 10px;
        font-size: 16px;
      }
    }
  }
  @media (min-width: 1920px) {
    .logo-wrap {
      flex: 0.67;
    }
    .wrap {
      flex: 1;
    }
    .navigation {
      flex: 1.53;
      &__item {
        margin-right: 40px;
        &:nth-child(2) {
          margin-right: 56px;
        }
      }
    }
  }
`;

export const BurgerMenu = styled('div')`
  position: absolute;
  top: 27px;
  right: 31px;
  display: inline-flex;
  flex-direction: column;
  justify-content: space-between;
  width: 29px;
  z-index: 1;
  .item {
    display: block;
    width: 100%;
    height: 4px;
    margin-bottom: 6px;
    background: #E5E5E5;
    transform-origin: center;
    transition: width .3s, transform .3s;
    &:first-child {
      transform: ${({open}) => open ? 'rotate(45deg) translate3d(6px, 8px, 0)' : 'rotate(0)'};
    }
    &:nth-child(2) {
      width: ${({open}) => open ? '0%' : '100%'};
    }
    &:last-child {
      transform: ${({open}) => open ? 'rotate(-45deg) translate3d(7px, -8px, 0)' : 'rotate(0)'};
      margin-bottom: 0;
    }
  }
  @media (min-width: 1024px) {
    top: 52px;
    right: 41px;
    &:hover {
      cursor: pointer;
    }
  }
  @media (min-width: 1280px) {
    display: none;
  }
`;